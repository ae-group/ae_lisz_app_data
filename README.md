<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# lisz_app_data 0.3.58

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_lisz_app_data/develop?logo=python)](
    https://gitlab.com/ae-group/ae_lisz_app_data)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_lisz_app_data/release0.3.57?logo=python)](
    https://gitlab.com/ae-group/ae_lisz_app_data/-/tree/release0.3.57)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_lisz_app_data)](
    https://pypi.org/project/ae-lisz-app-data/#history)

>ae_lisz_app_data package 0.3.58.

[![Coverage](https://ae-group.gitlab.io/ae_lisz_app_data/coverage.svg)](
    https://ae-group.gitlab.io/ae_lisz_app_data/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_lisz_app_data/mypy.svg)](
    https://ae-group.gitlab.io/ae_lisz_app_data/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_lisz_app_data/pylint.svg)](
    https://ae-group.gitlab.io/ae_lisz_app_data/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_lisz_app_data)](
    https://gitlab.com/ae-group/ae_lisz_app_data/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_lisz_app_data)](
    https://gitlab.com/ae-group/ae_lisz_app_data/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_lisz_app_data)](
    https://gitlab.com/ae-group/ae_lisz_app_data/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_lisz_app_data)](
    https://pypi.org/project/ae-lisz-app-data/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_lisz_app_data)](
    https://gitlab.com/ae-group/ae_lisz_app_data/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_lisz_app_data)](
    https://libraries.io/pypi/ae-lisz-app-data)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_lisz_app_data)](
    https://pypi.org/project/ae-lisz-app-data/#files)


## installation


execute the following command to install the
ae.lisz_app_data package
in the currently active virtual environment:
 
```shell script
pip install ae-lisz-app-data
```

if you want to contribute to this portion then first fork
[the ae_lisz_app_data repository at GitLab](
https://gitlab.com/ae-group/ae_lisz_app_data "ae.lisz_app_data code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_lisz_app_data):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_lisz_app_data/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.lisz_app_data.html
"ae_lisz_app_data documentation").
